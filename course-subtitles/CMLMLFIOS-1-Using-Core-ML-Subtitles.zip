PK
     :L@gZyK  K  /   1 - Machine Learning for iOS? - lang_en_vs1.srt1
00:00:00,000 --> 00:00:03,760
Machine learning on iOS has always been possible,

2
00:00:03,760 --> 00:00:07,290
but in iOS 11 it's been made easy with Core ML.

3
00:00:07,290 --> 00:00:11,700
But what is machine learning and how can you use it in your apps?

4
00:00:11,700 --> 00:00:13,630
Arthur Samuel, an AI pioneer,

5
00:00:13,630 --> 00:00:17,110
coined the term "machine learning" as the field of study that gives

6
00:00:17,110 --> 00:00:21,605
computers the ability to learn without being explicitly programmed.

7
00:00:21,605 --> 00:00:25,945
We can extend Samuel's definition to include apps that learn.

8
00:00:25,945 --> 00:00:28,710
You've probably already seen apps and features in

9
00:00:28,710 --> 00:00:32,075
iOS that use machine learning on a regular basis.

10
00:00:32,075 --> 00:00:36,645
Siri utilizes machine learning to help power your voice activated request.

11
00:00:36,645 --> 00:00:41,700
And the system's photo app uses machine learning for facial recognition.

12
00:00:41,700 --> 00:00:46,560
Pinterest uses machine learning to recommend items in your feed that you may like to pin.

13
00:00:46,560 --> 00:00:50,220
And Topology Eyewear, a custom eyeglass fitting company,

14
00:00:50,220 --> 00:00:53,040
has an iOS app that analyzes your selfie to build

15
00:00:53,040 --> 00:00:57,475
an accurate model of your face structure to create custom glasses for you.

16
00:00:57,475 --> 00:01:00,225
Each of these examples uses machine learning to provide

17
00:01:00,225 --> 00:01:03,715
a more personalized and meaningful user experience.

18
00:01:03,715 --> 00:01:05,825
So, how do you use machine learning in an app?

19
00:01:05,825 --> 00:01:08,790
Well first, you need a machine learning model.

20
00:01:08,790 --> 00:01:14,445
Machine learning model is a system that can output predictions given inputs.

21
00:01:14,445 --> 00:01:17,610
Most machine learning models are quite complex but with

22
00:01:17,610 --> 00:01:20,955
Core ML much of the complexity is abstracted away from you.

23
00:01:20,955 --> 00:01:24,360
Instead, you can think of a model like a black box.

24
00:01:24,360 --> 00:01:28,360
For example, let's look at how a model can be used to recognize images,

25
00:01:28,360 --> 00:01:30,825
a classic machine learning problem.

26
00:01:30,825 --> 00:01:32,995
Using our black box analogy,

27
00:01:32,995 --> 00:01:37,035
the model accepts an image as an input like this image of a dog.

28
00:01:37,035 --> 00:01:39,270
The model then outputs a prediction which is

29
00:01:39,270 --> 00:01:44,040
a statistical distribution of what the model predicts the image to be.

30
00:01:44,040 --> 00:01:47,790
We would expect the distribution to show the highest confidence prediction as a dog,

31
00:01:47,790 --> 00:01:52,000
in this case, and other predictions like the cat or turtle to have a low confidence.
PK
     :Lφ€υΠ	  Π	     2 - Training - lang_en_vs1.srt1
00:00:00,000 --> 00:00:03,420
You may be wondering how are machine learning models created,

2
00:00:03,420 --> 00:00:07,315
what enables them to make accurate predictions.

3
00:00:07,315 --> 00:00:09,600
The answer is training.

4
00:00:09,600 --> 00:00:14,675
Training is the process where the learning in machine learning really takes place.

5
00:00:14,675 --> 00:00:17,436
Let's consider an example.

6
00:00:17,436 --> 00:00:18,970
Here's a model.

7
00:00:18,970 --> 00:00:22,595
This model is a Convolutional Neural Network or CNN,

8
00:00:22,595 --> 00:00:24,970
a popular choice for classification problems

9
00:00:24,970 --> 00:00:27,895
like identifying the main object in an image.

10
00:00:27,895 --> 00:00:30,685
The structure of the CNN mimics a brain,

11
00:00:30,685 --> 00:00:33,160
where chemical and electrical synopsis quickly

12
00:00:33,160 --> 00:00:36,565
travel from neuron to neuron and elicit a response.

13
00:00:36,565 --> 00:00:39,475
Training a CNN is like training the brain.

14
00:00:39,475 --> 00:00:44,290
To get started, inputs pass through the model and outputs are produced.

15
00:00:44,290 --> 00:00:48,648
At first, the outputs are more often wrong than they are right,

16
00:00:48,648 --> 00:00:52,825
but each failure presents the opportunity for the CNN to correct itself.

17
00:00:52,825 --> 00:00:54,635
Using the brain analogy,

18
00:00:54,635 --> 00:00:57,730
the neurons correct themselves to make better predictions.

19
00:00:57,730 --> 00:01:01,345
For a CNN, the neuron changes correspond to changes

20
00:01:01,345 --> 00:01:05,050
in the mathematical calculations that describe the CNN.

21
00:01:05,050 --> 00:01:10,105
But don't worry, you don't need to be a math genius to use a CNN or a ConvNet.

22
00:01:10,105 --> 00:01:11,860
After training on many inputs,

23
00:01:11,860 --> 00:01:14,680
the CNN is optimized to make better predictions.

24
00:01:14,680 --> 00:01:17,020
We'd call it a trained model.

25
00:01:17,020 --> 00:01:20,455
You can use a trained model to make predictions or run inference,

26
00:01:20,455 --> 00:01:22,910
as it's commonly referred to.

27
00:01:22,910 --> 00:01:25,405
Since training requires running many samples,

28
00:01:25,405 --> 00:01:28,210
it's computationally expensive and time consuming,

29
00:01:28,210 --> 00:01:31,840
not a process well-suited to run on an iOS device,

30
00:01:31,840 --> 00:01:36,000
instead, trainings performed elsewhere like in the cloud or a dedicated machine.
PK
     :L2oY1  1     3 - Inference - lang_en_vs1.srt1
00:00:00,000 --> 00:00:01,920
Once we have a train model,

2
00:00:01,920 --> 00:00:05,790
we can use it to make predictions on our iOS device.

3
00:00:05,790 --> 00:00:08,685
The process of running inputs through your machine learning model,

4
00:00:08,685 --> 00:00:11,730
and obtaining a prediction as output is known as Inference.

5
00:00:11,730 --> 00:00:15,515
With the release of iOS 11 in Core ML,

6
00:00:15,515 --> 00:00:19,355
inference is just a few lines of code, more on this later.

7
00:00:19,355 --> 00:00:22,485
Before Core ML, inference was possible

8
00:00:22,485 --> 00:00:25,725
but it requires some heavy lifting to take a pre-trained model,

9
00:00:25,725 --> 00:00:28,730
and prepare it for use with existing frameworks.

10
00:00:28,730 --> 00:00:29,955
You had two options,

11
00:00:29,955 --> 00:00:34,025
Accelerate and Metals Performance Shaders, or MPS.

12
00:00:34,025 --> 00:00:36,510
We'll cover these later and show how they can be

13
00:00:36,510 --> 00:00:39,975
used to support email features prior to iOS 11.

14
00:00:39,975 --> 00:00:46,080
Accelerate and Metal Performance Shaders are still used under the hood of Core ML.

15
00:00:46,080 --> 00:00:49,680
As a developer, you can use Core ML as a developer friendly API,

16
00:00:49,680 --> 00:00:53,785
and Apple takes care of determining which underlying framework is used.

17
00:00:53,785 --> 00:00:57,640
Core ML detects what type of machine learning task you want to perform,

18
00:00:57,640 --> 00:01:01,570
and it decides of it should take advantage of running on the iPhone CPU,

19
00:01:01,570 --> 00:01:03,800
using Accelerate, or GPU,

20
00:01:03,800 --> 00:01:06,270
using Metal Performance Shaders.

21
00:01:06,270 --> 00:01:10,345
In other words, Apple does the performance optimization for you.

22
00:01:10,345 --> 00:01:16,105
Now, on device inference provides some unique advantages to developers and users.

23
00:01:16,105 --> 00:01:17,990
The first is Data Privacy.

24
00:01:17,990 --> 00:01:20,325
If you have the machine learning model on the device,

25
00:01:20,325 --> 00:01:24,925
then it stays there, and you can be sure that the user's data is kept there.

26
00:01:24,925 --> 00:01:27,600
The second is free computing power.

27
00:01:27,600 --> 00:01:30,540
If you're going to be using a machine learning as a service product,

28
00:01:30,540 --> 00:01:33,330
or spending up your own hardware to run inference,

29
00:01:33,330 --> 00:01:34,845
you're going to have to pay for that.

30
00:01:34,845 --> 00:01:37,170
Whereas if you're doing inference from the device,

31
00:01:37,170 --> 00:01:40,530
you can utilize the free computing power there.

32
00:01:40,530 --> 00:01:44,825
The third reason is that the model is always available if it's on the device.

33
00:01:44,825 --> 00:01:47,730
Since you're not relying on the network to run interference in the cloud,

34
00:01:47,730 --> 00:01:50,020
you don't have to worry if the connection drops.

35
00:01:50,020 --> 00:01:51,810
Even when the device is offline,

36
00:01:51,810 --> 00:01:55,710
you can be sure that it will be available because it doesn't use the network.

37
00:01:55,710 --> 00:01:57,570
Let's say you're using a machine learning model that

38
00:01:57,570 --> 00:02:00,390
recognizes birds for the purpose of bird-watching,

39
00:02:00,390 --> 00:02:04,050
and you're deep in the woods with little to no cellular connection.

40
00:02:04,050 --> 00:02:06,525
You'd still be able to take a photo of the bird and run

41
00:02:06,525 --> 00:02:10,000
inference on the device determine which type of bird it is.
PK
     :LIηGBm&  m&  /   4 - Using Core ML in Your App - lang_en_vs1.srt1
00:00:00,000 --> 00:00:03,315
Now that we've covered some of the fundamentals of machine learning,

2
00:00:03,315 --> 00:00:06,523
we can get started with building an app that uses Core ML.

3
00:00:06,523 --> 00:00:10,255
We've all seen a standard grocery list app.

4
00:00:10,255 --> 00:00:12,480
This grocery list app is built by H.J.

5
00:00:12,480 --> 00:00:15,045
Goldman. In a grocery list app,

6
00:00:15,045 --> 00:00:17,070
we can display items that a user needs to buy at

7
00:00:17,070 --> 00:00:21,140
the grocery store and we can add items to that list.

8
00:00:21,140 --> 00:00:23,400
Wouldn't it be cool instead of just simply typing

9
00:00:23,400 --> 00:00:25,830
in the items we wanted to that we could snap

10
00:00:25,830 --> 00:00:30,785
a photo like a photo of an orange and add it to the list automatically?

11
00:00:30,785 --> 00:00:33,060
We could build this feature using Core ML.

12
00:00:33,060 --> 00:00:37,125
But first, we need a Core ML model that recognizes images.

13
00:00:37,125 --> 00:00:40,560
We could use one of Apple's pre-treated Core ML models.

14
00:00:40,560 --> 00:00:46,485
If you look at this page on their website on developer.apple.com/machine-learning,

15
00:00:46,485 --> 00:00:48,925
there's a section on these Intel models.

16
00:00:48,925 --> 00:00:50,370
So we see ResNet,

17
00:00:50,370 --> 00:00:56,190
Places205, and MobileNet. We can use MobileNet.

18
00:00:56,190 --> 00:00:59,490
It's a standard image recognition model and it's able to recognize

19
00:00:59,490 --> 00:01:05,025
the dominant object present in image from 1,000 different categories.

20
00:01:05,025 --> 00:01:08,915
So let's download this Core ML model for MobileNet.

21
00:01:08,915 --> 00:01:10,245
Now, while this is downloading,

22
00:01:10,245 --> 00:01:13,865
I should say that this model is actually pretty reasonably sized.

23
00:01:13,865 --> 00:01:15,810
It's at 17 megabytes.

24
00:01:15,810 --> 00:01:18,120
But if you look at say ResNet,

25
00:01:18,120 --> 00:01:20,190
which does essentially the same thing,

26
00:01:20,190 --> 00:01:24,360
it will recognize the dominant object for 1,000 different categories.

27
00:01:24,360 --> 00:01:26,370
It's 102 megabytes.

28
00:01:26,370 --> 00:01:31,740
So, MobileNet will be much more size efficient than ResNet and some of the others.

29
00:01:31,740 --> 00:01:33,540
Once it's finished downloading,

30
00:01:33,540 --> 00:01:35,410
we'll see it in our downloads folder.

31
00:01:35,410 --> 00:01:37,695
And it's in the.mlmodel file format,

32
00:01:37,695 --> 00:01:41,885
which is Core ML's representation of a machine learning model.

33
00:01:41,885 --> 00:01:46,195
The MLModel can be dragged directly into Xcode.

34
00:01:46,195 --> 00:01:49,560
Note, you'll need to be running on Xcode 9 or higher to be able to use

35
00:01:49,560 --> 00:01:53,990
this feature and the new iOS 11 frameworks like Core ML.

36
00:01:53,990 --> 00:01:56,475
Also, this project that I'm dragging the model into,

37
00:01:56,475 --> 00:01:57,900
I provided it ahead of time.

38
00:01:57,900 --> 00:02:00,060
It's called The Smart Grocery List app,

39
00:02:00,060 --> 00:02:03,300
and you can download it at the link I put alongside this video.

40
00:02:03,300 --> 00:02:04,830
So after dragging in the file,

41
00:02:04,830 --> 00:02:08,100
we can accept these default options and click "Finish",

42
00:02:08,100 --> 00:02:10,875
and we want to make sure that MobileNet

43
00:02:10,875 --> 00:02:14,550
has membership and are smart grocery lists target.

44
00:02:14,550 --> 00:02:17,843
So we can see where MobileNet has been added to our project.

45
00:02:17,843 --> 00:02:20,550
And if we select it, then we can see a lot of metadata about

46
00:02:20,550 --> 00:02:24,810
the model such as its name, author, and description.

47
00:02:24,810 --> 00:02:28,260
Also, here at the bottom, we have a section called Model Evaluation Parameters,

48
00:02:28,260 --> 00:02:30,165
and I apologize for it being small.

49
00:02:30,165 --> 00:02:32,820
But you can see that this section tells us the type of

50
00:02:32,820 --> 00:02:36,120
inputs and outputs that the machine learning model expects.

51
00:02:36,120 --> 00:02:37,860
So, for the MobileNet model,

52
00:02:37,860 --> 00:02:40,470
it expects an image as input and it has

53
00:02:40,470 --> 00:02:43,935
two outputs: class label probabilities and class label.

54
00:02:43,935 --> 00:02:47,875
Class label probs is a dictionary that list each of the categories

55
00:02:47,875 --> 00:02:52,815
the MobileNet model has to categorize images and then the corresponding probability.

56
00:02:52,815 --> 00:02:58,215
And class label is a string that gives the name of the most likely image category.

57
00:02:58,215 --> 00:03:01,455
So let's see this model in action using Core ML.

58
00:03:01,455 --> 00:03:03,160
If we go to our main view control,

59
00:03:03,160 --> 00:03:04,995
the recognizer view controller,

60
00:03:04,995 --> 00:03:08,370
we can use this model directly in source code.

61
00:03:08,370 --> 00:03:13,540
So I'll start typing let MobileNet equal,

62
00:03:13,540 --> 00:03:16,460
and there we see a class of MobileNet.

63
00:03:16,460 --> 00:03:18,680
Now, where is this class coming from?

64
00:03:18,680 --> 00:03:20,870
If we go back to our MLModel file,

65
00:03:20,870 --> 00:03:22,380
we can see this little arrow which is

66
00:03:22,380 --> 00:03:26,295
a swift generated interface for model under the model class section.

67
00:03:26,295 --> 00:03:29,035
So let's click that arrow.

68
00:03:29,035 --> 00:03:33,500
Here are the classes that Xcode automatically generated for us.

69
00:03:33,500 --> 00:03:35,510
So we see there's a MobileNet input,

70
00:03:35,510 --> 00:03:40,805
MobileNet output, and then our class MobileNet here.

71
00:03:40,805 --> 00:03:43,615
Let's go back to that main view controller.

72
00:03:43,615 --> 00:03:49,115
At this point, we've already cleared a property with our MobileNet model.

73
00:03:49,115 --> 00:03:53,265
And next, we need to look at the recognize grocery item method.

74
00:03:53,265 --> 00:03:56,730
This is where we want to make our prediction about what the grocery item

75
00:03:56,730 --> 00:04:01,675
is based on the image and then we can add that to our grocery list if we want to.

76
00:04:01,675 --> 00:04:03,860
So we'll scroll down a bit.

77
00:04:03,860 --> 00:04:07,190
And here's our recognized function.

78
00:04:07,190 --> 00:04:09,830
And it takes a UIImages input,

79
00:04:09,830 --> 00:04:14,745
and that's the photo that we've just taken or maybe a photo from our photo library.

80
00:04:14,745 --> 00:04:17,240
So we need to take this UIImage and convert it to

81
00:04:17,240 --> 00:04:22,070
a CV pixel buffer so that it can be used by the model's prediction function.

82
00:04:22,070 --> 00:04:26,510
And we've provided a helper method to do this conversion for you.

83
00:04:26,510 --> 00:04:29,683
It's in this ImageToPixelBufferConverter file.

84
00:04:29,683 --> 00:04:32,710
And we can see the static function convert to pixel buffer.

85
00:04:32,710 --> 00:04:36,440
It takes a UIImage and gives us a pixel buffer.

86
00:04:36,440 --> 00:04:38,825
So we can go ahead and use this function.

87
00:04:38,825 --> 00:04:41,390
And if it gives us a pixel buffer,

88
00:04:41,390 --> 00:04:43,493
then we'll go ahead and continue processing,

89
00:04:43,493 --> 00:04:45,495
otherwise will return nil.

90
00:04:45,495 --> 00:04:49,085
And assuming we have a valid pixel buffer image,

91
00:04:49,085 --> 00:04:51,650
then we can call the MobileNet's prediction function,

92
00:04:51,650 --> 00:04:53,910
and we'll pass it that pixel buffer image.

93
00:04:53,910 --> 00:04:58,760
And this also needs to use the try statement because it can throw.

94
00:04:58,760 --> 00:05:02,315
Now, assuming all of this executes without failure,

95
00:05:02,315 --> 00:05:04,055
then we'll have a prediction,

96
00:05:04,055 --> 00:05:05,915
and we can use the class label,

97
00:05:05,915 --> 00:05:08,983
which is the category with the highest confidence,

98
00:05:08,983 --> 00:05:10,995
and we'll return that string.

99
00:05:10,995 --> 00:05:13,070
And if we return a string here,

100
00:05:13,070 --> 00:05:16,250
the rest of the code is already wired up so that it gives us the opportunity

101
00:05:16,250 --> 00:05:19,400
to either add that to a grocery list or say,

102
00:05:19,400 --> 00:05:21,200
no, we don't want add it to the grocery list.

103
00:05:21,200 --> 00:05:24,315
So let's go ahead and run this and see what happens.

104
00:05:24,315 --> 00:05:27,400
And since we're going to be testing this in the simulator,

105
00:05:27,400 --> 00:05:29,380
we'll have to use the photo library.

106
00:05:29,380 --> 00:05:30,445
We can't use our camera,

107
00:05:30,445 --> 00:05:32,395
but if you were running this on a real device,

108
00:05:32,395 --> 00:05:34,840
then you could use your camera and test this out.

109
00:05:34,840 --> 00:05:36,795
So here's the main interface.

110
00:05:36,795 --> 00:05:40,570
We'll click the photo icon to use a photo library.

111
00:05:40,570 --> 00:05:42,735
Just give it a second to load.

112
00:05:42,735 --> 00:05:45,837
I've already loaded this image of an orange.

113
00:05:45,837 --> 00:05:47,745
So we'll select it.

114
00:05:47,745 --> 00:05:50,775
It's a little hard to see, but the label there is already saying orange.

115
00:05:50,775 --> 00:05:52,340
So it recognize this as an orange.

116
00:05:52,340 --> 00:05:57,095
And we can either reject or accept this being added to our shopping list.

117
00:05:57,095 --> 00:05:58,385
So we'll click the "Plus",

118
00:05:58,385 --> 00:06:01,240
an orange is now added to the shopping list.
PK
     :La©:%/  /  .   5 - Using Vision With CoreML - lang_en_vs1.srt1
00:00:00,000 --> 00:00:02,583
When building the smart grocery list app,

2
00:00:02,583 --> 00:00:06,470
we use Core ML to recognize photos of food items in your kitchen.

3
00:00:06,470 --> 00:00:09,230
And then, we can add those items to our grocery list.

4
00:00:09,230 --> 00:00:13,080
The image classification was performed by MobileNet which was

5
00:00:13,080 --> 00:00:16,850
generated from the MobileNet.mlmodel file.

6
00:00:16,850 --> 00:00:19,845
You can access the MobileNet class by going to MobileNet.mlmodel

7
00:00:19,845 --> 00:00:24,815
and then go into the model class and clicking this arrow.

8
00:00:24,815 --> 00:00:29,755
This takes you to the generated file where we see class MobileNet.

9
00:00:29,755 --> 00:00:32,580
The MobileNet class has a prediction function which takes

10
00:00:32,580 --> 00:00:35,970
a CV pixel buffer as input and returns a dictionary

11
00:00:35,970 --> 00:00:38,970
full of predictions or the key value as a class label or the name

12
00:00:38,970 --> 00:00:42,495
of an object and its probability of existing in an image.

13
00:00:42,495 --> 00:00:46,845
An alias for that dictionary is this MobileNet output type.

14
00:00:46,845 --> 00:00:52,175
To use this function we needed the image to pixel buffer converter helper class.

15
00:00:52,175 --> 00:00:57,990
This function converted UI images into CV pixel buffers but it has a lot of overhead.

16
00:00:57,990 --> 00:01:00,220
First, we have to resize the image.

17
00:01:00,220 --> 00:01:03,370
And then, converting to a pixel buffer uses a lot of low level

18
00:01:03,370 --> 00:01:07,270
graphics API's that most developers are not accustomed to using.

19
00:01:07,270 --> 00:01:09,670
These unfamiliar concepts and code can be

20
00:01:09,670 --> 00:01:12,335
quite involved and they're more prone to failure.

21
00:01:12,335 --> 00:01:16,240
Wouldn't it be easier if we could just call the prediction function using a UI image?

22
00:01:16,240 --> 00:01:18,715
Well, with the vision framework, we can.

23
00:01:18,715 --> 00:01:21,595
The vision framework works together with Core ML to perform

24
00:01:21,595 --> 00:01:25,860
the image preprocessing necessary for image classification.

25
00:01:25,860 --> 00:01:29,235
The vision framework also has features for face detection,

26
00:01:29,235 --> 00:01:31,635
bar code detection, object tracking.

27
00:01:31,635 --> 00:01:35,760
But for this example, we're going to focus on image preprocessing.

28
00:01:35,760 --> 00:01:37,920
Feel free to refer back to this documentation on

29
00:01:37,920 --> 00:01:40,050
the Apple developer website if you're interested

30
00:01:40,050 --> 00:01:42,580
in further exploring visions capabilities.

31
00:01:42,580 --> 00:01:44,506
I'll post a link alongside this video.

32
00:01:44,506 --> 00:01:49,165
Let's get started with using vision and Core ML in our smart grocery list app.

33
00:01:49,165 --> 00:01:50,880
I'm going to switch over to a new project in

34
00:01:50,880 --> 00:01:55,440
the Core ML repository and the folder vision plus Core ML Init.

35
00:01:55,440 --> 00:01:59,590
In this project, go to the recognizer view controller class.

36
00:01:59,590 --> 00:02:04,395
Like before, this view controller is responsible for initiating image classification.

37
00:02:04,395 --> 00:02:07,750
First, let's import the vision framework at the top of the file.

38
00:02:07,750 --> 00:02:12,190
Now, there are five simple steps to perform image classification with vision.

39
00:02:12,190 --> 00:02:17,780
The first involves creating the vision model or an instance of the VNCoreML model class.

40
00:02:17,780 --> 00:02:21,880
Then, we need to create a request, a VNCoreML request.

41
00:02:21,880 --> 00:02:24,910
Next, we'll create the request handler and call perform on

42
00:02:24,910 --> 00:02:28,930
that handler using the original Core ML request.

43
00:02:28,930 --> 00:02:32,110
Finally, we handle the food classification results.

44
00:02:32,110 --> 00:02:35,620
You can see these steps outlined in the classified food function as well as

45
00:02:35,620 --> 00:02:40,150
the handle food classification results for that final fifth step.

46
00:02:40,150 --> 00:02:43,000
So, let's start with number one and create the vision model.

47
00:02:43,000 --> 00:02:48,550
This vision model or the VNCoreML model class is a wrapper around the MobileNet's model.

48
00:02:48,550 --> 00:02:52,561
Because the VNCoreML model initializer can throw an exception,

49
00:02:52,561 --> 00:02:55,390
we'll use the try and guard statement together

50
00:02:55,390 --> 00:02:58,850
to safely ensure that this model class is initialized.

51
00:02:58,850 --> 00:03:01,570
Otherwise, we'll return a fatal error.

52
00:03:01,570 --> 00:03:03,040
Next, we create the request.

53
00:03:03,040 --> 00:03:05,272
To initialize a VNCoreML request,

54
00:03:05,272 --> 00:03:07,840
we need a vision model and a completion handler that will

55
00:03:07,840 --> 00:03:11,110
dictate what happens with the request results.

56
00:03:11,110 --> 00:03:13,000
We already have the model we just initiated

57
00:03:13,000 --> 00:03:16,000
the VNCoreML model and then for the completion handler,

58
00:03:16,000 --> 00:03:18,295
we've already created a function called handle

59
00:03:18,295 --> 00:03:21,410
food classification results that we can use for that.

60
00:03:21,410 --> 00:03:25,420
I've created this temporary code snippet just to represent the completion handler.

61
00:03:25,420 --> 00:03:27,960
So, notice how the signature of the completion handler takes

62
00:03:27,960 --> 00:03:33,125
a VN request and then an optional error type and it returns nothing.

63
00:03:33,125 --> 00:03:37,885
This is the same signature as the handle food classification results that we have below.

64
00:03:37,885 --> 00:03:39,910
Next, then we need to create a request handler.

65
00:03:39,910 --> 00:03:41,020
Note, this is different than

66
00:03:41,020 --> 00:03:45,430
the completion handler that we saw above despite sounding similar.

67
00:03:45,430 --> 00:03:48,790
The type of handler is a VN image request handler and

68
00:03:48,790 --> 00:03:52,705
it takes a cgImage and an orientation to initialize one.

69
00:03:52,705 --> 00:03:55,080
For the cgImage, this is actually pretty easy,

70
00:03:55,080 --> 00:03:58,360
we can just use the UI images cgImage property.

71
00:03:58,360 --> 00:04:00,985
Because that property is optional and it can be nil,

72
00:04:00,985 --> 00:04:04,090
we'll wrap the initialization in a guard statement.

73
00:04:04,090 --> 00:04:09,240
Next, we need the orientation which should be of type cgImage property orientation.

74
00:04:09,240 --> 00:04:12,550
We can use one line of code to capture the proper orientation value.

75
00:04:12,550 --> 00:04:14,440
We just do a little manipulation of

76
00:04:14,440 --> 00:04:19,495
the images image orientation value converting that into the proper type.

77
00:04:19,495 --> 00:04:23,380
I should mention it's really important to initialize the orientation correctly because

78
00:04:23,380 --> 00:04:28,650
classification isn't reliable if vision doesn't know the exact orientation of the image.

79
00:04:28,650 --> 00:04:30,970
Okay, the first three steps are done so lets call

80
00:04:30,970 --> 00:04:33,595
perform on the request handler with the request.

81
00:04:33,595 --> 00:04:36,010
Perform takes an array of requests, but in our case,

82
00:04:36,010 --> 00:04:37,460
we just have one,

83
00:04:37,460 --> 00:04:39,820
the food classification request.

84
00:04:39,820 --> 00:04:44,045
The perform function can throw errors so we've wrapped it in a do catch block.

85
00:04:44,045 --> 00:04:46,880
And if we do catch any errors we'll print them.

86
00:04:46,880 --> 00:04:50,470
Now, the perform method can also take a bit of time so we want to make sure that

87
00:04:50,470 --> 00:04:54,535
we're dispatching this work to a queue that's not blocking the main queue.

88
00:04:54,535 --> 00:04:56,692
To dispatch this work on a separate queue,

89
00:04:56,692 --> 00:04:59,260
we'll use the dispatch queue global and

90
00:04:59,260 --> 00:05:02,127
then for the quality of service of the queue that we want,

91
00:05:02,127 --> 00:05:03,884
we'll go with user initiated.

92
00:05:03,884 --> 00:05:06,280
This will give us feedback quickly but it will still be off

93
00:05:06,280 --> 00:05:10,780
the main queue and then dot async followed by the work that we want to do.

94
00:05:10,780 --> 00:05:13,233
When the request handler is done performing the request,

95
00:05:13,233 --> 00:05:14,710
we should get the results back with

96
00:05:14,710 --> 00:05:18,940
the food classifications in our completion handler that we specified earlier.

97
00:05:18,940 --> 00:05:23,470
Remember, that is the handle food classification results function.

98
00:05:23,470 --> 00:05:28,075
Recall the inputs for the function are a VN request and an optional error.

99
00:05:28,075 --> 00:05:30,430
We can get the image classifications from

100
00:05:30,430 --> 00:05:34,210
the request results and notice we'll use a guard statement

101
00:05:34,210 --> 00:05:37,150
again to safely pull out the classifications which are

102
00:05:37,150 --> 00:05:40,480
this array of VN classification observations.

103
00:05:40,480 --> 00:05:45,085
And then, we'll take the top classification which is the first entry in that array.

104
00:05:45,085 --> 00:05:46,840
If there's not a first object,

105
00:05:46,840 --> 00:05:50,290
we'll show an alert in the UI to let the user know.

106
00:05:50,290 --> 00:05:54,250
Otherwise, we'll display the first prediction to the user by calling our setup prediction

107
00:05:54,250 --> 00:05:58,585
function and passing at that top classification identifier.

108
00:05:58,585 --> 00:06:03,070
The identifier property gives us the prediction as a string value.

109
00:06:03,070 --> 00:06:06,835
Finally, we should remember to dispatch this work on the main queue since we'll be

110
00:06:06,835 --> 00:06:11,965
updating the UI and this completion handler is called from a background queue.

111
00:06:11,965 --> 00:06:14,190
So, we need to go from background to main.

112
00:06:14,190 --> 00:06:17,773
Now that we've implemented image classification using vision and Core ML,

113
00:06:17,773 --> 00:06:20,405
we just need to update our means of classification in

114
00:06:20,405 --> 00:06:24,185
the image picker controller did finished picking media callback.

115
00:06:24,185 --> 00:06:26,780
If we scroll down to the bottom of the recognized view

116
00:06:26,780 --> 00:06:29,910
controller source file, we find this function.

117
00:06:29,910 --> 00:06:31,440
We can remove the existing call to

118
00:06:31,440 --> 00:06:36,380
the recognized function and replace it with the call to the classified food function.

119
00:06:36,380 --> 00:06:40,655
We can also remove the existing recognize function as it's no longer needed with

120
00:06:40,655 --> 00:06:44,728
our new way of doing image classification with vision and Core ML.

121
00:06:44,728 --> 00:06:50,330
So, let's scroll up and find that function, here it is.

122
00:06:50,330 --> 00:06:52,362
And we can get rid of that.

123
00:06:52,362 --> 00:06:57,130
This also applies to our helper class where converting UI images to pixel buffers

124
00:06:57,130 --> 00:07:02,640
so we can completely remove this file, there we go.

125
00:07:02,640 --> 00:07:07,465
Now, let's build the app and see how our new way of doing classification performs.

126
00:07:07,465 --> 00:07:10,410
Just like before because we can't test a camera in the simulator,

127
00:07:10,410 --> 00:07:14,040
we'll use an image from the photo library as a test.

128
00:07:14,040 --> 00:07:17,080
Let's use this picture of a pineapple.

129
00:07:17,080 --> 00:07:20,088
I apologize for the way this text reads it's kind of hard on the simulator.

130
00:07:20,088 --> 00:07:23,205
But you can see that pineapple was recognized in the image.

131
00:07:23,205 --> 00:07:25,140
And we can add that to our shopping list.
PK-
     :L@gZyK  K  /           €    1 - Machine Learning for iOS? - lang_en_vs1.srtPK-
     :Lφ€υΠ	  Π	             €  2 - Training - lang_en_vs1.srtPK-
     :L2oY1  1             €€  3 - Inference - lang_en_vs1.srtPK-
     :LIηGBm&  m&  /           €$  4 - Using Core ML in Your App - lang_en_vs1.srtPK-
     :La©:%/  /  .           €ΜJ  5 - Using Vision With CoreML - lang_en_vs1.srtPK      ―  ¬z    